# Test no 3 - API Automation
1. Query yang saya lakukan karena yang ingin ditampilkan adalah nama character female dengan status masih alive, dengan menampilkan name, ID, Status, Species, Gender.
Sehingga saya memfilter berdasarkan gender dan status

2. A. Saya menggunakan filter sama dengan soal nomor 1 untuk menampilkan langsung nama yang di inginkan
   B. Dari Query character saya menggunakan ID Location yang di dapatkan dari query Character untuk menampilkan Location dari character       tersebut
   C. Saya juga menampilkan List Episode yang terdapat di Character tersebut dan karakter tersebut memiliki 1 episode

# Test no 4 - Query question
Dari soal yang diberikan diketahui table employee terdiri dari 
-employee_id
-employee_name
-buddy_id
-supervisor_id
-team_name

Dari atribut yang ada dalam table employee dapat di simpulkan terdapat 2 foreign key
yang mengarah ke dua table yaitu
-Table buddy
-Table supervisor

Sehingga query untuk mendapatkan data yang diinginkan seperti berikut

`
Select * from Employee e join Buddy b on e.buddy_id = b.id join Supervisor s on E.supervisor_id
	
Select e.employee_id, e.employee_name, b.buddy_name, s.supervisor_name, e.team_name from Employee e join Buddy b on e.buddy_id = b.id join Supervisor s on e.supervisor_id
`